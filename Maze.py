import random as r


class Cell:
    '''Maze Generation: Prim's Algorithm'''
    objects = {}
    list = []

    def __init__(self, raw, cell, b=True):
        self.raw = raw
        self.cell = cell
        self.pos = '{}_{}'.format(raw, cell)
        self.str_cell = '{}{}{}'
        self.r_wall = True
        self.b_wall = True
        if b:
            Cell.objects[self.pos] = self

    def __call__(self):
        if self.b_wall:
            b = '_'
        else:
            b = ' '
        if self.r_wall:
            r = '|'
        else:
            r = ' '
        return self.str_cell.format(b,b,r)

    def get_neighbors(self):
        return [_() for _ in [self.left_cell, self.top_cell, self.right_cell, self.bottom_cell] if _()]

    def chenge_border(self, other):
        if self.cell > other.cell and self.raw == other.raw:
            other.r_wall = False
        if self.cell == other.cell and self.raw > other.raw:
            other.b_wall = False
        if self.cell < other.cell and self.raw == other.raw:
            self.r_wall = False
        if self.cell == other.cell and self.raw < other.raw:
            self.b_wall = False

    def left_cell(self):
        try:
            return Cell.objects['{}_{}'.format(self.raw, self.cell-1)]
        except KeyError:
            return None

    def top_cell(self):
        try:
            return Cell.objects['{}_{}'.format(self.raw-1, self.cell)]
        except KeyError:
            return None

    def right_cell(self):
        try:
            return Cell.objects['{}_{}'.format(self.raw, self.cell+1)]
        except KeyError:
            return None

    def bottom_cell(self):
        try:
            return Cell.objects['{}_{}'.format(self.raw+1, self.cell)]
        except KeyError:
            return None


class Maze:
    def __init__(self, raws, cells):
        self.my_seed = r.randint(10000, 1000000)
        self.raws = raws
        self.cells = cells
        self.my_obj_cell = Cell

    def set_seed(self, seed):
        self.my_seed = seed
        r.seed(seed)

    def get_seed(self):
        return self.my_seed

    def gen_maze(self, post_chenge=True):
        r.seed(self.my_seed)

        maze_map = [[self.my_obj_cell(raw, cell) for cell in range(1, self.cells+1)] for raw in range(1, self.raws+1)]

        map_list = [j for i in maze_map for j in i]


        road = []#Масив пути
        temp_cells = []#Временный масив ячеек

        curent_cell = r.choice(map_list)#Случайная ячейка

        temp_cells.extend(curent_cell.get_neighbors())#Добавить в временный массив соседние ячейки

        road.append(curent_cell)#Добавить ячейку в путь

        map_list.remove(curent_cell)#Удалить ячейку из общего массива

        while len(map_list) != 0:
            curent_cell = r.choice(temp_cells)#Выбрать случайную ячейку из временного массива
            road_cell = r.choice([t for t in curent_cell.get_neighbors() if t in road])#Найти ячейки которые входят в массив пути и выбрать случайную
            curent_cell.chenge_border(road_cell)#Разрушить границу между ячейками
            road.append(curent_cell)#Добавить ячейку в путь
            temp_cells.extend([t for t in curent_cell.get_neighbors() if not t in temp_cells and not t in road])#Добавить в временный массив соседние ячейки которые не входят в путь и временный массив
            temp_cells.remove(curent_cell)#Удалить ячейку из временного массива

            map_list.remove(curent_cell)#Удалить ячейку из общего массива

        if post_chenge:
            for i in range(1, len(maze_map)+1):
                t_cell = self.my_obj_cell(i, 0, b=False)
                t_cell.b_wall = False
                maze_map[i-1].insert(0, t_cell)

            c = []
            for i in range(self.cells+1):
                t_cell = self.my_obj_cell(0, i, b=False)
                if i > 0:
                    t_cell.r_wall = False
                else:
                    t_cell.r_wall = False
                    t_cell.b_wall = False
                c.append(t_cell)
            maze_map.insert(0, c)

        #print('len', len(map_list))

        '''
        for _c in maze_map:
            for _r in _c:
                print(_r(), end='')
            print()
        '''
        return maze_map



if __name__ in '__main__':
    maze = Maze(15, 15)
    s1 = maze.gen_maze()

    for c in s1:
        for r in c:
            print(r(), end='')
        print()
    #print(s1[10][15])
