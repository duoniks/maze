import time
import random as r
from tkinter import *

from Maze import Cell, Maze


WIDTH = 640
HEIGHT = 480
SIZE_CELL = 18
C_WIDTH = WIDTH
C_HEIGHT = HEIGHT

root = Tk()
root.minsize(width = WIDTH, height = HEIGHT)

def new_map():
    main()

def main():
    print('New mep, ', end=' ')
    c = Canvas(root, width=C_WIDTH, height=C_HEIGHT, bg='#000000')
    c.grid(row=1,column=0,columnspan=3)
    maze = MyMaze(int((C_HEIGHT-SIZE_CELL)/SIZE_CELL), int((C_WIDTH-SIZE_CELL-SIZE_CELL)/SIZE_CELL))
    global player
    player = Player()
    maze.set_seed(maze.get_seed())
    labelText.set('seed: '+str(maze.get_seed()))
    print('seed: ', maze.get_seed())
    m = maze.gen_maze()
    for row in m:
        for cell in row:
            cell.get(c)
    player.spaun(c, root, m)

but = Button(root, text='Exit')
but['command'] = root.destroy
but.grid(row=0, column=0, sticky='nsew')

but1 = Button(root, text = 'NEW')
but1['command'] = new_map
but1.grid(row=0, column=1, sticky='nsew')

labelText = StringVar()
labelText.set('Hello')
lbl = Label(root, textvariable=labelText)
lbl.grid(row=0,column=2)


class MyCell(Cell):
    def __init__(self, raws, cells, b=True):
        super().__init__(raws, cells, b)


    def get(self, parent):

        if self.b_wall:# _
            parent.create_line((self.cell*SIZE_CELL), (self.raw*SIZE_CELL)+SIZE_CELL,
                                (self.cell*SIZE_CELL)+SIZE_CELL, (self.raw*SIZE_CELL)+SIZE_CELL,
                                fill='#00ff00', width=2)
        '''
        else:
            parent.create_line((self.cell*SIZE_CELL), (self.raw*SIZE_CELL)+SIZE_CELL,
                                (self.cell*SIZE_CELL)+SIZE_CELL, (self.raw*SIZE_CELL)+SIZE_CELL,
                                fill='#000000', width=2)
        '''
        if self.r_wall:# |
            parent.create_line((self.cell*SIZE_CELL)+SIZE_CELL, (self.raw*SIZE_CELL)+SIZE_CELL,
                                (self.cell*SIZE_CELL)+SIZE_CELL, (self.raw*SIZE_CELL),
                                fill='#00ff00', width=2)
        '''
        else:
            parent.create_line((self.cell*SIZE_CELL)+SIZE_CELL, (self.raw*SIZE_CELL)+SIZE_CELL,
                                (self.cell*SIZE_CELL)+SIZE_CELL, (self.raw*SIZE_CELL),
                                fill='#000000', width=2)
        '''

class MyMaze(Maze):
    def __init__(self, raws, cells):
        super().__init__(raws, cells)
        self.my_obj_cell = MyCell


class Player():
    def __init__(self):
        self.size = SIZE_CELL + 2
        self.size2 = SIZE_CELL + SIZE_CELL - 4
        self.p1 = C_WIDTH-SIZE_CELL-SIZE_CELL
        self.p2 = C_HEIGHT-SIZE_CELL
        self.x, self.y, self.x1, self.y1 = self.size, self.size, self.size2, self.size2
        self.player_spaun = int(r.randint(1, int((C_HEIGHT-SIZE_CELL)/SIZE_CELL)-1))
        self.finish = int(r.randint(1, int((C_HEIGHT-SIZE_CELL)/SIZE_CELL)-1))

        self.cord_finish = [int(self.size+(int((C_WIDTH-SIZE_CELL-SIZE_CELL)/SIZE_CELL)-1)*SIZE_CELL),
                            self.size+self.finish*SIZE_CELL,
                            int(self.size2+(int((C_WIDTH-SIZE_CELL-SIZE_CELL)/SIZE_CELL)-1)*SIZE_CELL),
                            self.size2+self.finish*SIZE_CELL]

    def spaun(self, parent, root, map):
        self.root = root
        self.t_start = time.time()

        #print(int((C_HEIGHT-SIZE_CELL)/SIZE_CELL), C_HEIGHT)
        #print(int((C_WIDTH-SIZE_CELL-SIZE_CELL)/SIZE_CELL), C_WIDTH)
        #print(self.size*int((C_HEIGHT-SIZE_CELL)/SIZE_CELL), self.size*int((C_WIDTH-SIZE_CELL-SIZE_CELL)/SIZE_CELL))
        self.map = map
        self.parent = parent
        self.parent.create_oval(*self.cord_finish, width=1, fill='#ff00ff')
        self.y += (self.player_spaun*SIZE_CELL)
        self.y1 += (self.player_spaun*SIZE_CELL)
        self.id = self.parent.create_oval(self.x, self.y, self.x1, self.y1, width=1, fill='#ffff00')
        self.root.bind('<Key>', self.move)
        #self.root.bind('<Motion>', self.move_andr)#For ANDROID

    def move_andr(self, ev):
        x, y = ev.x, ev.y
        state = ev.state


        if ev.char == 'a':
            if not self.map[int((self.y-2)/SIZE_CELL)][int((self.x-2)/SIZE_CELL)-1].r_wall:
                self.x -= SIZE_CELL
                self.x1 -= SIZE_CELL
        if ev.char == 'w':
            if not self.map[int((self.y-2)/SIZE_CELL)-1][int((self.x-2)/SIZE_CELL)].b_wall:
                self.y -= SIZE_CELL
                self.y1 -= SIZE_CELL
        if ev.char == 'd':
            if not self.map[int((self.y-2)/SIZE_CELL)][int((self.x-2)/SIZE_CELL)].r_wall:
                self.x += SIZE_CELL
                self.x1 += SIZE_CELL
        if ev.char == 's':
            if not self.map[int((self.y-2)/SIZE_CELL)][int((self.x-2)/SIZE_CELL)].b_wall:
                self.y += SIZE_CELL
                self.y1 += SIZE_CELL

        if int((self.x-2)/SIZE_CELL)==int((self.cord_finish[0]-2)/SIZE_CELL) and int((self.y-2)/SIZE_CELL)==int((self.cord_finish[1]-2)/SIZE_CELL):
            self.parent.delete(self.id)
            self.parent.destroy()
            t = time.time()-self.t_start
            labelText.set('YOU WIN LOSE TIME: {:02}:{:02} min.'.format(int(t//60), int(t%60)))
            print('YOU WIN LOSE TIME: {:02}:{:02} min.'.format(int(t//60), int(t%60)))
            main()
        else:
            self.parent.delete(self.id)
            self.id = self.parent.create_oval(self.x, self.y, self.x1, self.y1, width=1, fill='#ffff00')

    def move(self, ev):

        if ev.char == 'a' or ev.char == 'A' or ev.keysym == 'Left':
            if not self.map[int((self.y-2)/SIZE_CELL)][int((self.x-2)/SIZE_CELL)-1].r_wall:
                self.x -= SIZE_CELL
                self.x1 -= SIZE_CELL
        if ev.char == 'w' or ev.char == 'W' or ev.keysym == 'Up':
            if not self.map[int((self.y-2)/SIZE_CELL)-1][int((self.x-2)/SIZE_CELL)].b_wall:
                self.y -= SIZE_CELL
                self.y1 -= SIZE_CELL
        if ev.char == 'd' or ev.char == 'D' or ev.keysym == 'Right':
            if not self.map[int((self.y-2)/SIZE_CELL)][int((self.x-2)/SIZE_CELL)].r_wall:
                self.x += SIZE_CELL
                self.x1 += SIZE_CELL
        if ev.char == 's' or ev.char == 'S' or ev.keysym == 'Down':
            if not self.map[int((self.y-2)/SIZE_CELL)][int((self.x-2)/SIZE_CELL)].b_wall:
                self.y += SIZE_CELL
                self.y1 += SIZE_CELL

        if int((self.x-2)/SIZE_CELL)==int((self.cord_finish[0]-2)/SIZE_CELL) and int((self.y-2)/SIZE_CELL)==int((self.cord_finish[1]-2)/SIZE_CELL):
            self.parent.delete(self.id)
            self.parent.destroy()
            t = time.time()-self.t_start
            labelText.set('YOU WIN LOSE TIME: {:02}:{:02} min.'.format(int(t//60), int(t%60)))
            print('YOU WIN LOSE TIME: {:02}:{:02} min.'.format(int(t//60), int(t%60)))
            main()
        else:
            self.parent.delete(self.id)
            self.id = self.parent.create_oval(self.x, self.y, self.x1, self.y1, width=1, fill='#ffff00')


if __name__ == "__main__":
    main()
    root.mainloop()
